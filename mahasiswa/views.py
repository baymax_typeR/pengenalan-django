from django.shortcuts import render
from . import models

# Create your views here.

dataMhs = models.Mahasiswa.objects.all()

context = { 'title' : 'Belajar Django',
            'judul' : 'Web Mahasiswa',
            'pageName' : 'Halaman Tampil Mahasiswa',
            'kontributor' : 'Isyana Wikrama D. T.',
            'mahasiswas' : dataMhs }

def index(request):
    return render(request, 'mahasiswa/index.html', context)