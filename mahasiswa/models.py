from __future__ import unicode_literals
from django.db import models

from django.contrib.auth.models import User

class Mahasiswa(models.Model):
    JK_CHOICES = (('l', 'Pria'), ('w', 'Wanita'))
    PS_CHOICES = (('mi', 'Manajemen Informatika'), ('ak', 'Akuntansi'), ('me', 'Mesin'))

    nama = models.CharField('Nama Mahasiswa', max_length=50)
    alamat = models.CharField(max_length=100)
    jk = models.CharField('Kelamin', max_length=1, choices=JK_CHOICES)
    ps = models.CharField('Program Studi', max_length=2, choices=PS_CHOICES)
    nim = models.CharField(max_length=30)
    no_tlp = models.CharField('Nomor HP', max_length=30, blank=True)
    email = models.CharField('Email', max_length=100, blank=True)

    def __unicode__(self):
        return self.nama
